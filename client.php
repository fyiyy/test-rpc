<?php


use Fy\Rpc\RpcClient;


class client
{

    public function test()
    {
        include_once 'vendor/autoload.php';

        $config = [
            'host' => '192.168.0.5',
            'port' => 9502,
            'timeOut' => 5
        ];

        $client = new RpcClient($config);
        $result = $client->setClass('server')->index(['code' => 1, 'msg' => 'server']);
        var_dump($result);
        $result = $client->setClass('server')->sayHello('Swoole');
        var_dump($result);
        $result = $client->setClass('app\test')->index(['code' => 1, 'msg' => 'app\test']);
        var_dump($result);
        $result = $client->setClass('server')->index(['code' => 1, 'msg' => 'server']);
        var_dump($result);
        $result = $client->setClass('server')->sayHello1('111111');
        var_dump($result);
    }

}

$obj = new client();

$obj->test();
